#!/bin/bash


# set -x
set -e

INPUT="./roms-7z"
OUTPUT_GOOD="./output_good"
OUTPUT_BAD="./tmp/output_bad"

mkdir -p $OUTPUT_GOOD
mkdir -p $OUTPUT_BAD

for filename in "$INPUT"/*.7z; do
    echo "Extracting $filename" in $OUTPUT_GOOD
    7z x -o$OUTPUT_GOOD "$filename"
done

echo "Keeping only good dumps with [!] and licensed"
for filename in "$OUTPUT_GOOD"/*; do
    if [[ ("$filename" != *"[!]"*) || ("$filename" == *"(Unl)"*) ]]; then
        echo "Moving $filename to $OUTPUT_BAD"
        mv "$filename" "$OUTPUT_BAD"
    else
        echo "Keeping $filename"
    fi
done
